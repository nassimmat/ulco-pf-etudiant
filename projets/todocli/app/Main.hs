import Text.Read
import System.Environment
import System.IO
import Data.List
import System.Exit


menu :: String -> IO ()
--menu "print"= prints
menu "exit"=exit


type Task = (Int, Bool, String)     -- id, done?, label
type Model = (Int, [Task])          -- next id, tasks

test :: Model -> Model
test (nextI, tasks) = (nextI, reverse tasks)


exit :: IO ()
exit = do 
    exitSuccess


main :: IO ()
main = do
    putStr"usage : "
    args <- getLine
    contents <- readFile args



    let model1 = read args
        model2 = test $! model1
    print model2
    mapM_ print (snd model2)
  

   --non terminer

