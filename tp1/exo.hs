main :: IO ()
main = do
    putStrLn "What's your name"
    input <- getLine
    if input ==""
        then putStrLn ("goodbye")
        else do
            putStrLn ("Hello " ++ input ++"!")
            main