borneEtFormate :: Double -> String
borneEtFormate x0 = show x1
    where
        x1 = if x0 < 0 then x0

borneEtFormate' :: Double -> String
borneEtFormate' x0 = show x1
    let
        x1 = if x0 < 0 then x0 
    in show x1

borneEtFormate1 :: Double -> String
borneEtFormate1 x0 = show x1 ++ " -> " ++ show x2
    where
        x1 = if x0 < 0 then 0 else x0
        x2 = if x1 < 0 then 1 else x1


borneEtFormate2 :: Double -> String
borneEtFormate2 x0 = show x1 ++ " -> " ++ show x2
    where
        x1 | x0 < 0 = 0
           | x0 > 1 = 0
           | otherwise = x0

main :: IO ()
main = do
    print (borneEtFormate1 (-1))