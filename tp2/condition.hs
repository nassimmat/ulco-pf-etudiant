formaterParite :: Int -> String
formaterParite x = if even x then "pair" else "impair"

formaterSigner :: int -> String 
formaterSigner x =
    if x == 0 :
        then "nul"
        else if x > 0
            then "positif"
            else "negatif"

formaterParite' :: Int -> String
formaterParite' x
    | even x = "pair"
    |otherwise = "impair"


main :: IO ()
main = do
    print(formaterParite 22)
    print( formaterSigner 21)
    print(formaterParite' 23)
    