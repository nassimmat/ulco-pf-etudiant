fibo :: Int -> Int
fibo x =
    case x of
        0 -> 0
        1 -> 1
        x -> fibo (x-1) + fibo(x-2)



fibo' :: Int -> Int

fibo' 0 = 0
fibo' 1 = 1
fibo' x = fibo' (x-1) + fibo'(x-2)

main :: IO ()
main = do
    print (fibo 0)
    print (fibo 12)

    print (fibo' 9)
    print (fibo' 12)
    