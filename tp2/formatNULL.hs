formatnull :: Int -> String
formatnull n = show  n ++ " est " ++ fmt n
    where
        fmt x = case x of 
            0 -> "NUL"
            
            _-> "non null"
formatnull2 :: Int -> String
formatnull2 n = show  n ++ " est " ++ fmt n
    where
        fmt 0 ="NUL"
            
        fmt  _ ="non null"

main :: IO ()
main = do
    print(formatnull 0)
    print(formatnull 3)

    print(formatnull2 0)
    print(formatnull2 3)