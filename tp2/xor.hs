xor1 :: Bool -> Bool -> Bool
xor1 x y =  
    if x 
        then if y then False else True
        else if y then True else False
testXor :: (Bool -> Bool -> Bool) -> Bool
testXor f 
    =  f True True == False
    && f True False == True
    && f False True == True
    && f True True == False

xor2 :: Bool -> Bool -> Bool
xor2 x y = x /= y

xor3 :: Bool -> Bool -> Bool



xor3 x y = if x then not y else y 

xor4 :: Bool -> Bool -> Bool
xor4 x y = case (x, y ) of
    (True,False) -> True
    (False,True) -> True
    _-> False

xor5 :: Bool -> Bool -> Bool
xor5 x y
    | x && not y  = True
    | not x && y = True
    | otherwise = False

xor6 :: Bool -> Bool -> Bool
xor6  True False = True
xor6  False True = True
xor6  _ _ = False


main :: IO ()
main = do
    print(xor1 False False)
    print(xor1 False True)
    print(xor1 True False)
    print (xor1 True True)

    print("----")

    print (testXor xor2 )


