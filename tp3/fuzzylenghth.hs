fuzzyLength :: [a] -> String
fuzzyLength l =
    case length l of
        0 -> "empty"
        1 -> "one"
        2 -> "two"
        ->"many"

main :: IO ()
main = do
    print (fuzzyLength [])
    print (fuzzyLength [1,2])
    print (fuzzyLength [1..4])
{-
fuzzyLength [] = "empty"
fuzzyLength [_] = "one" {-underscore-}
fuzzyLength [_,_] = "two" 

-}
    

