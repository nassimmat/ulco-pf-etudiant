import System.Environment

countlines :: String -> Ints
countlines str =length (lines str) 

main :: IO ()
main = do
    args <- getArgs
    case  args of 
        [filename]-> do
            content <- readFile filename
            putStrLn ("lines :"++countlines content )

            ...
        -> putStrLn "usage : <file>"
   