facttco :: Int -> Int 
facttco x = go 1 x
    where  
        go acc 1 = acc
        go acc n = go (acc*n) (n-1)

main :: IO ()
main = do
    print (facttco 4)