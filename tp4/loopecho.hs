loopEcho :: Int -> IO String
loopEcho 0 = return "loop terminated"
loopEcho n = do
    putStrLn"on est dans loopEcho"
    return " message retourne par loupEcho"
    mot <- getLine
    if mot == ""
        then return "empty line"
        else do
            putStrLn mot
            loopEcho (n-1)
    
main :: IO ()
main = do
    msg <- loopEcho 3
    putStrLn msg
