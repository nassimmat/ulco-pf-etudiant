doubler :: [Int] -> [Int]
doubler xs =  [x*2|x<-xs]

mymap :: (a -> b) -> [a] -> [b] 
mymap f xs = [f x | x <- xs]

pairs :: [Int] -> [Int]
pairs xs = [ x |x<-xs, even x]

myfilter :: (a -> Bool) -> [a] -> [a]
myfilter p xs = [x | x <- xs ,p x]

multiples :: Int -> [Int]
multiples 0 = [0]
multiples n = [x| x <- [1..n], n`mod` x == 0]

combinaisons :: [a] ->[b] -> [(a,b)]
combinaisons xs ys = [ (x,y) | x<-xs, y<-ys]

tripletsPyth :: Int ->[(int,int,int)]
tripletsPyth n = [(x,y,z) |x <- [1..n],y<-[x..n],z<-[y..n],x^2+y^2==z^2]

main :: IO ()
main = do
    print (doubler [1..4])
    print (pairs [1..4])
   


