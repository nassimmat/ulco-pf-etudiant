
import Data.Char

filterEvent1 :: [Int] -> [Int]
filterEvent1 [] = []
filterEvent1 (x:xs) = 
    if even x
        then x: filterEvent1 xs
        else filterEvent1 xs 

filterEvent2 :: [Int] -> [Int]
filterEvent2 xs = filter even xs

myfilter :: (Int -> Bool) -> [a]-> [a]
myfilter [] = 0
myfilter f (x:xs) = 
    if f x
        then x: myfilter xs
        else myfilter xs

main :: IO ()
main = do
    print (filterEvent1 [1..4])
    print (filterEvent2 [1..4])
    print (myfilter even [1..4])
    print (myfilter isLetter "toto12")
