import Data.Char

foldSum1 :: [Int] -> Int
foldSum1 [] = 0
foldSum1 (x:xs) = x + foldSum1 xs

foldSum2 :: [Int] -> Int
foldSum2 xs = foldl (\ acc x -> acc + x ) 0 xs

myfoldl :: (Int ->a -> Int) -> Int -> [a]-> Int
myfoldl f acc [] = acc
myfoldl f acc (x:xs) = myfoldl f (f acc x) xs

main :: IO ()
main = do
    print (foldSum1[1..4])
    print (foldSum2[1..4])

    print(myfoldl (+) 0 [1..4])
    print (myfoldl max 'a' "barfoo" )
