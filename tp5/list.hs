myConcat1 :: [[a]] ->[a]
myConcat1 xss =foldl (++) [] xss

myConcat2 :: [[a]]->[a]
myConcat2 xss = [x | xs<-xss, x <-xs]

getHead1 :: [[a]] -> [a]
getHead1 xss = map head xss 


getHead2 :: [[a]] -> [a]
getHead2 xss = [head xs |xs <- xss]


main :: IO ()
main = do
print (myConcat1 ["foo", "bar"])

print (myConcat2 ["foo", "bar"])

print (getHead1 ["foo", "bar"])
print (getHead2 ["foo", "bar"])
