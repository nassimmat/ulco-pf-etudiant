plus42positif :: Int->Bool
---plus42positif x = (x+42) > 0
plus42positif = (>0).(+42)
--- >>> plus42positif 2
--- True
---

mul2PlusUn :: Int->Int
mul2PlusUn = (+1).(*2)

mul2MoinsUn :: Int->Int
mul2MoinsUn = (flip(-)1).(*2)

applyTwice :: (a -> a) -> a -> a
applyTwice f = f.f


main :: IO ()
main = do
    print(mul2PlusUn 2)
    print(mul2MoinsUn 2)
    print()