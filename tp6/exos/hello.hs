 
myRangeTuple1:: (Int,Int)->[Int]
myRangeTuple1 (x, xs)=[x..xs]

myRangeCurry1:: Int->Int->[Int]
myRangeCurry1 x xs =[x..xs]

myRangeTuple2 :: Int->[Int]
myRangeTuple2 x = myRangeTuple1(0,x) 

myRangeCurry2 :: Int ->[Int]
myRangeCurry2 x = myRangeCurry1 0 x
--myRangeCurry2  = myRangeCurry1 0 


myTake2 :: [Int]->[Int]
myTake2 x = take 2 x

main :: IO ()
main = do

    print (myRangeTuple1(1,9))
    print (myRangeCurry1 1 9)
    print (myRangeTuple2 9)
    print (myRangeCurry2 9)
    
    print(myTake2 [1..7])

